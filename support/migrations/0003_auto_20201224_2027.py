# Generated by Django 3.1.4 on 2020-12-24 13:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('support', '0002_auto_20201224_2024'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='SupportModel',
            new_name='Support',
        ),
    ]
