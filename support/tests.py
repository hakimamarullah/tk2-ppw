from django.test import TestCase, Client
from support.models import Support
from django.apps import apps
from support.apps import SupportConfig
from django.contrib.auth.models import User
import json

# Create your tests here.

class SupportConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(SupportConfig.name, 'support')
        self.assertEqual(apps.get_app_config('support').name, 'support')

class SupportTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            password='johnpassword')

    def test_url_status_200(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/formulirsupport/')
        self.assertEqual(200, response.status_code)
        response = self.client.get('/landingsupport/')
        self.assertEqual(200, response.status_code)

    def test_template_formulir_support(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/formulirsupport/')
        self.assertTemplateUsed(response, 'formulirsupport.html')
    
    def test_template_landing_support(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/landingsupport/')
        self.assertTemplateUsed(response, 'landingsupport.html')
    
    def test_view_formulir_support(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/formulirsupport/')
        isi_html_respon = response.content.decode ('utf8')
        self.assertIn("Kirimkan", isi_html_respon)
    
    def test_form_formulir_support(self):
        self.client.login(username='john', password='johnpassword')
        self.client.post('/formulirsupport/', data={'email' : 'ayuendri@gmail.com', 'pesan' : 'Saya ingin mengetahui hasil pemeriksanaan'})
        jumlah = Support.objects.filter(email='ayuendri@gmail.com').count()
        self.assertEqual(jumlah, 1)

    def test_string_representation_matkul(self):
        self.client.login(username='john', password='johnpassword')
        object_support = Support(email="unit_test@yahoo.com")
        self.assertEqual(str(object_support), object_support.email)

    def test_view_formulir_kegiatan(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/landingsupport/')
        isi_html_respon = response.content.decode ('utf8')
        self.assertIn("Pesan Anda Telah Terkirim", isi_html_respon)
        self.assertIn("Terima kasih telah mengirim pesan kepada kami! Kami akan segera mengirimkan jawaban melalui email Anda", isi_html_respon)
        self.assertIn("Kirim Pesan Lagi", isi_html_respon)
        self.assertIn("Temukan pesan lama mu!", isi_html_respon)

    def test_template_call_ajax(self):
        response = self.client.get('/AjaxSupportDetail/')
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['content-type'], 'text/html; charset=utf-8')
    
    def test_type_return_JSON_support(self):
        response = self.client.get('/supportJSONData/')
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['content-type'], 'application/json')

class APITest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='john', 
            password='johnpassword')

    def test_manage_JSON(self):
        self.client.login(username='ayu', password='Maung2019')
        response = self.client.get('/supportJSONData/')
        string_response_body = response.content.decode("utf-8")

        json_response_body = json.loads(string_response_body)

        self.assertIn('username', json_response_body[0])

