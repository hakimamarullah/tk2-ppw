from django.test import TestCase, Client
from .models import Articles
from .forms import ArticlesForm
from django.core.files import File
from django.core.files.uploadedfile import SimpleUploadedFile
from django.contrib.auth.models import User

# Class Test Utama
class UnitTest(TestCase):

    # Menguji apakah urlnya dapat diakses

    def setUp(self):
        user = User.objects.create_user(username='john', password='johnpassword')
        user2 = User.objects.create_user(username='sena', password='senapassword')
        user.save()
        user2.save()
        gambar = SimpleUploadedFile(name='test_image.jpg',
                                    content=open('articles/static/image/chizuru.jpg', 'rb').read(),
                                    content_type='image/jpeg')
        Articles.objects.create(penulis="john", kategori="Umum", tanggal="2020-11-23", judul="Sebuah Artikel",
                                isi_artikel="Test", gambar_artikel=gambar)

    def test_url_is_exist(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/articles/')
        self.assertEqual(response.status_code, 200)

    def test_using_template(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/articles/')
        self.assertTemplateUsed(response, "articles.html")

    # Menguji kelengkapan dari halaman artikel
    def test_kelengkapan_artikel(self):
         self.client.login(username='john', password='johnpassword')
         response= self.client.get('/articles/')
         isi_html_artikel = response.content.decode('utf8')
         self.assertIn("Semua Artikel", isi_html_artikel)
         self.assertIn("DUAR", isi_html_artikel)
         self.assertIn("Contact", isi_html_artikel)
         self.assertIn("Assesment", isi_html_artikel)
         self.assertIn("Articles", isi_html_artikel)
         self.assertIn("Support", isi_html_artikel)
         self.assertIn("Feedback", isi_html_artikel)

    # Menguji apakah model bekerja dengan benar
    def test_model_artikel(self):
        jumlah_artikel = Articles.objects.all().count()
        self.assertEqual(jumlah_artikel, 1)
        sebuah_artikel = Articles.objects.get(id = 1)
        self.assertEqual("Sebuah Artikel", sebuah_artikel.__str__())

    # Menguji apakah form sudah lengkap
    def test_form_artikel(self):
        self.client.login(username='john', password='johnpassword')
        response = self.client.get('/insertarticles/')
        response2 = self.client.get('/insertAjaxArticles/')
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response2.status_code, 200)
        isi_html_artikel = response.content.decode('utf8')
        self.assertIn('Tambahkan Artikel', isi_html_artikel)
        self.assertContains(response, 'id_penulis')
        self.assertContains(response, 'id_kategori')
        self.assertContains(response, 'id_tanggal')
        self.assertContains(response, 'id_judul')
        self.assertContains(response, 'id_isi_artikel')
        self.assertContains(response, 'id_gambar_artikel')

    # Menguji apakah form dapat bekerja sesuai yang diinginkan
    def test_insert_artikel(self):
        self.client.login(username='john', password='johnpassword')
        gambar_mock = SimpleUploadedFile(name='test_image.jpg',
                                    content=open('articles/static/image/jokowi.jpg', 'rb').read(),
                                    content_type='image/jpeg')
        args = {
            'penulis': 'Jokowi',
            'kategori': 'Kesehatan',
            'tanggal': '2030-11-25',
            'judul': 'Penanganan Covid Pemerintah',
            'isi_artikel': 'Memasuki tahun 2020, dunia diguncang oleh wabah virus korona yang menyebar dengan sangat cepat ke seluruh dunia. Hal ini mendorong pemerintah Indonesia untuk melakukan upaya dan mengambil kebijakan penanganan virus korona. Salah satu tindakan awal yang dilakukan oleh Presiden Joko Widodo saat itu adalah dengan memerintahkan kedutaan Indonesia di China.',
            'gambar_artikel': gambar_mock
        }

        response_post = self.client.post('/insertarticles/', args)
        self.assertEqual(response_post.status_code, 302)

        jumlah_artikel = Articles.objects.all().count()
        self.assertEquals(2, jumlah_artikel)

        jokowi = Articles.objects.get(id = 2)

        response = self.client.get('/articles/')
        self.assertEquals(response.status_code, 200)
        isi_html_artikel = response.content.decode('utf8')
        self.assertIn(jokowi.judul, isi_html_artikel)
        self.assertIn(jokowi.penulis, isi_html_artikel)
        self.assertIn("Nov. 25, 2030", isi_html_artikel)
        self.assertIn("Memasuki tahun 2020,", isi_html_artikel)

        response_selengkapnya = self.client.get('/articles/2')
        self.assertEquals(response.status_code, 200)
        isi_html_selengkapnya = response_selengkapnya.content.decode('utf8')
        self.assertIn(jokowi.judul, isi_html_selengkapnya)
        self.assertIn(jokowi.penulis, isi_html_selengkapnya)
        self.assertIn("Nov. 25, 2030", isi_html_selengkapnya)
        self.assertIn(jokowi.isi_artikel, isi_html_selengkapnya)

    # Menguji apakah artikel dapat dihapus sesuai keinginan
    def test_hapus_artikel(self):
        self.client.login(username='john', password='johnpassword')
        cnt = Articles.objects.all().count()
        response_konfirmasi = self.client.get(f'/deletearticle/{Articles.objects.all()[0].id}')
        self.assertEqual(response_konfirmasi.status_code, 200)
        response = self.client.post(f'/deletearticle/{Articles.objects.all()[0].id}')
        self.assertEqual(Articles.objects.all().count(), cnt - 1)
        self.assertEqual(response.status_code, 302)

    def test_fail_hapus_artikel(self):
        self.client.login(username='sena', password='senapassword')
        cnt = Articles.objects.all().count()
        response = self.client.post(f'/deletearticle/{Articles.objects.all()[0].id}')
        self.assertEqual(Articles.objects.all().count(), cnt)
        self.assertEqual(response.status_code, 200)

    def test_API_call(self):
        response = self.client.get("/articlesAPI/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['content-type'], 'application/json')

    def test_Ajax_post(self):
        self.client.login(username='sena', password='senapassword')
        gambar_mock = SimpleUploadedFile(name='test_image.jpg',
                                    content=open('articles/static/image/jokowi.jpg', 'rb').read(),
                                    content_type='image/jpeg')
        args = {
            'penulis': 'Jokowi',
            'kategori': 'Kesehatan',
            'tanggal': '2030-11-25',
            'judul': 'Penanganan Covid Pemerintah',
            'isi_artikel': 'Memasuki tahun 2020, dunia diguncang oleh wabah virus korona yang menyebar dengan sangat cepat ke seluruh dunia. Hal ini mendorong pemerintah Indonesia untuk melakukan upaya dan mengambil kebijakan penanganan virus korona. Salah satu tindakan awal yang dilakukan oleh Presiden Joko Widodo saat itu adalah dengan memerintahkan kedutaan Indonesia di China.',
            'gambar_artikel': gambar_mock
        }

        self.client.post('/insertAjaxArticles/', args)
        jumlah_artikel = Articles.objects.all().count()
        self.assertEquals(2, jumlah_artikel)

