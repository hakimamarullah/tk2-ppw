$(document).ready(function () {
    var input_field = $("#keyword");
    $(".btn_kanan , .btn_kiri").click(function(event) {
        const button_sekarang = $(this);
        const blok_artikel = button_sekarang.parent().parent();
        if (button_sekarang.is(".btn_kiri")) {
            blok_artikel.insertBefore(blok_artikel.prev());
        } else {
            blok_artikel.insertAfter(blok_artikel.next());
        }
        });
    $(".searchButton").click(function () {
        const form = $("#keyword");
        form.css('display', 'unset');
        $(this).remove();
        $(".searchButton2").css('display', 'unset')
    });

    $(".searchButton2").click(function () {
        $.ajax({
            url: "/articlesAPI/",
            success: searchVal,
            error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
          }

        })
    });

    input_field.keyup(function (e) {
        if (e.which === 13) {
        $.ajax({
            url: "/articlesAPI/",
            success: searchVal,
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }

        })
    }
    });

    function searchVal(data) {
        var container = $(".container_articles");
        var username = data[data.length - 1];
        var ketikan = input_field.val();
        container.empty();
        var counter = 0;
        for (i = 0; i < data.length - 1; i++) {
                    var penulis = data[i].penulis;
                    var tanggal = data[i].tanggal;
                    var pk = data[i].pk;
                    var url_artikel = "/articles/" + pk;
                    var url_delete = "/deletearticle/" + pk;
                    var judul = data[i].judul;
                    var isi = data[i].isi_artikel;
                    var isi_pendek = jQuery.trim(isi).substring(0, 130).trim(this) + "...";
                    var gambar = data[i].gambar_artikel;
                    var ketemu = judul.toLowerCase().search(ketikan.toLowerCase());
                    if (ketemu === -1) {
                        counter = counter + 1;
                    } else {
                        if (username.username !== penulis) {
                            container.append(
                                "<div class='single_artikel'>" +
                                "<div class='container_gambar'>" + "<img class='gambar_artikel' src=" + gambar +
                                "></div>" +
                                "<div class='isi_artikel'>" + "<p class='judul_artikel'>" + judul + "</p>" +
                                "<p class='isi_artikel'>" + isi_pendek + "</p>" +
                                "</div>" +
                                "<div class='hapus_dan_lanjutan'>" +
                                "<a href=" + url_artikel + "><button class='btn btn-info' id='btnselengkapnya'>Selengkapnya</button>" +
                                "</a>" +
                                "</div>" +
                                "<div class='tanggal_kategori'>" +
                                "<p class='kategori'> Oleh <span style='color:red;'>" + penulis + "</span>" + "</p>" +
                                "<p class='tanggal_main'> 🕐" + tanggal + "</p>" +
                                "</div>" +
                                "</div>"
                            )
                        } else {
                            container.append(
                                "<div class='single_artikel'>" +
                                "<div class='container_gambar'>" + "<img class='gambar_artikel' src=" + gambar +
                                "></div>" +
                                "<div class='isi_artikel'>" + "<p class='judul_artikel'>" + judul + "</p>" +
                                "<p class='isi_artikel'>" + isi_pendek + "</p>" +
                                "</div>" +
                                "<div class='hapus_dan_lanjutan'>" +
                                "<a href=" + url_artikel + "><button class='btn btn-info' id='btnselengkapnya'>Selengkapnya</button>" +
                                "</a>" +
                                "<a href=" + url_delete + "><button class='btn btn-danger' id='btnHapusmain'>Hapus Artikel</button>" +
                                "</a>" +
                                "</div>" +
                                "<div class='tanggal_kategori'>" +
                                "<p class='kategori'> Oleh <span style='color:red;'>" + penulis + "</span>" + "</p>" +
                                "<p class='tanggal_main'> 🕐" + tanggal + "</p>" +
                                "</div>" +
                                "</div>"
                            )
                        }
                    }
                }
                if (counter === data.length - 1) {
                    container.append("<p style='color: white;'> Artikel yang ada cari tidak ada </p>")
                }
    }
    
});