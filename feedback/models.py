from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.core.validators import MaxValueValidator, MinValueValidator
class Feedback(MPTTModel):
	user = models.CharField(max_length=35, default='anonymous')
	created = models.DateTimeField(auto_now_add=True)
	body = models.TextField()
	score = models.IntegerField(default=0, null=False, blank=False,

			validators = [
				MaxValueValidator(5),
				MinValueValidator(0),
			]

		)
	parent = TreeForeignKey('self', null=True, blank=True, on_delete=models.CASCADE, related_name='children')

	class MPTTMeta :
		ordering = ['-score']

	def __str__(self):
		return f'Feedback by {self.user}'