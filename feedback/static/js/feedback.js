//SELECT ALL STARS
const one = document.getElementById('first')
const two = document.getElementById('seconds')
const three = document.getElementById('third')
const four = document.getElementById('fourth')
const five = document.getElementById('fifth')

const form = document.querySelector('.form-group')
const csrf = document.getElementsByName('csrfmiddlewaretoken')
const button = document.getElementById('submit-btn')

const arr = [one, two, three, four, five]
let score;
//COLORING THE STAR
const handleSelectedStar = (size) =>{
	for(let i=2; i<form.children.length; i++ ){
		if(i <= size+1){
			form.children[i].classList.add("checked")
		}
		else{
			form.children[i].classList.remove("checked")
		}
	}
}

const getNumericValue = (str) =>{
	let num;
	if(str === 'first'){
		num =1
	}
	else if(str === 'seconds'){
		num=2
	}
	else if(str === 'third'){
		num = 3

	}
	else if(str === 'fourth'){
		num = 4
	}
	else if(str === 'fifth'){
		num = 5
	}
	else{
		num =0;
	}
	return num
}
const handleSelectStar = (selection) =>{
	switch(selection){
		case "first":{
			handleSelectedStar(1)
			return
		}
		case "seconds":{
			handleSelectedStar(2)
			return
		}
		case "third":{
			handleSelectedStar(3)
			return
		}
		case "fourth":{
			handleSelectedStar(4)
			return
		}
		case "fifth":{
			handleSelectedStar(5)
			return
		}
	}
}

arr.forEach(item=> item.addEventListener('click', (event)=>{
	handleSelectStar(event.target.id)
	score = getNumericValue(event.target.id)

	button.addEventListener('click', e =>{
			e.preventDefault()
			document.getElementById('id_score').value = score

			$.ajax({
				type:'POST',
				url:'/feedback/',
				data:{
					'csrfmiddlewaretoken':csrf[0].value,
					'score':document.getElementById('id_score').value,
					'body':document.getElementById('id_body').value,
					'user':document.getElementById('id_user').value,
				},
				success: function(response){
					alert("Success")
				}
			})
		})

	
}))

