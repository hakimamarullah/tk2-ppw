from django import forms
from django.forms import ModelForm, Textarea, TextInput, DateInput, PasswordInput
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from crispy_forms.helper import FormHelper

class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'email', 'password1', 'password2')
        widgets = {'username': TextInput(attrs={'class':'form-control', 'placeholder': 'Username'}),
                   'first_name': TextInput(attrs={'class':'form-control', 'placeholder': 'Full Name'}),
                   'email': TextInput(attrs={'class':'form-control', 'placeholder': 'Email  '}),
        }

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget = PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password from numbers and letters of the Latin alphabet'})
        self.fields['password2'].widget = PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password confirmation'})