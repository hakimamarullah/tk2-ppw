# Generated by Django 3.1.4 on 2020-12-31 07:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kuesioner', '0004_auto_20201031_0004'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kuesioner',
            name='Nama',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
