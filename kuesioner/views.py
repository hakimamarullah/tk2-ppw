import kuesioner
from django.shortcuts import render, redirect
from kuesioner.forms import *
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
# Create your views here.

@login_required(login_url='/login')
def formulir_kuesioner(request):
    form = KuesionerForm(initial={"Nama": request.user.first_name})
    if request.method == "POST":
        form = KuesionerForm(request.POST)
        if form.is_valid():
            hasil, created = Kuesioner.objects.update_or_create(Nama= request.user.first_name, defaults={**form.cleaned_data})
            return redirect('/hasilkuesioner/' + str(hasil.pk))
    context = {
        'form' : form
    }
    return render(request, 'kuesioner.html', context)

@login_required(login_url='/login')
def list_kuesioner(request):
    return render(request, 'list_kuesioner.html')

@login_required(login_url='/login')
def hasil_kuesioner(request, pk):
    hasilKuesioner = Kuesioner.objects.get(id=pk)
    context = {
        "hasil_kuesioner": hasilKuesioner
    }
    return render(request, 'hasil_kuesioner.html', context)

@login_required(login_url='/login')
def query_kuesioner(request):
    qs = list(Kuesioner.objects.values())
    context = {
        "kuesioner": qs
    }
    return JsonResponse(context)
