$(document).ready(function () {
    $("#kirim").hover(function(){
        $(this).css({"background-color": "#FCDAB7", "color": "black"});
        }, function(){
        $(this).css({"background-color": "#1E5F74", "color": "white"});
    });

    $(".list_kuesioner").hover(function(){
        $(this).css({"background-color": "#1E5F74"});
        $(".a_list_kuesioner").css({"color": "white"});
        }, function(){
        $(this).css({"background-color": "#FCDAB7"});
        $(".a_list_kuesioner").css({"color": "black"});
    });

    $("#show_recommendation").hover(function(){
        $(this).css({"background-color": "#FCDAB7", "color": "black"});
        }, function(){
        $(this).css({"background-color": "#1E5F74", "color": "white"});
    });

    $("#show_recommendation").click(function() {
        $(".container_recommendation_main").fadeToggle("slow");
        $(this).text(function(i, text){
            return text === "Lihat Rekomendasi" ? "Tutup Rekomendasi" : "Lihat Rekomendasi";
        })
    });

    $("#search_kuesioner").click(function() {
        var keyword = $("#data_kuesioner").val();
        // console.log(keyword)
        $.ajax( {
            type: "GET",
            url: "/jsonkuesioner/",
            success: function(hasil) {
                // console.log("HORE")
                var kuesioner_container = $("#kuesioner_container");
                kuesioner_container.empty();
                var no_kuesioner_container = $("#no_kuesioner_container");
                no_kuesioner_container.empty();
                var counter = 0;
                // console.log(hasil.kuesioner);
                for(i = 0; i < hasil.kuesioner.length; i++) {
                    if(hasil.kuesioner[i].Nama.includes(keyword)) {
                        counter += 1;
                        var Name = hasil.kuesioner[i].Nama
                        var ID = hasil.kuesioner[i].id
                        kuesioner_container.append(`<tr><td class='query_kuesioner_individu'><a class='nama_list' href='/hasilkuesioner/${ID}/'>${Name}</a></td></tr>`);
                    }
                }
                if(counter == 0) {
                    no_kuesioner_container.append("Maaf, data yang Anda cari tidak ditemukan!");
                }
                // for(i = 0; i < hasil.result.length; i++) {
                //     console.log(hasil.kuesioner[i])
                //     if(hasil.kuesioner[i].Nama.includes(keyword)) {
                //         kuesioner_container.append("<td><a class='nama_list' href='{% url 'hasilkuesioner' hasil.id %}'>{{ hasil.Nama }}</a></td>{% if hasil.hitung_nilai == 0 or hasil.hitung_nilai == 1 or hasil.hitung_nilai == 2 or hasil.hitung_nilai == 3 or hasil.hitung_nilai == 4 %}<td class='kecil_sedang'>Kecil - Sedang</td>{% else %}<td class='besar'>Besar</td>{% endif %}")
                //     }
                // }
            }
        })
    });
    $(document).on({
        mouseenter: function() {
            $(this).css('color', 'blue');
        }, mouseleave: function() {
            $(this).css('color', 'black');
        }
    }, ".nama_list");
});